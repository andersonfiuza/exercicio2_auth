package br.com.itau.DTO;

import br.com.itau.Model.Contato;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

public class ContatoEntradaDTO {


    private int usuarioid;

    private String nome;
    private String telefone;

    public int getUsuarioid() {
        return usuarioid;
    }

    public void setUsuarioid(int usuarioid) {
        this.usuarioid = usuarioid;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Contato ConverterParaContato() {
        Contato contato = new Contato();
        contato.setUsuarioid(this.getUsuarioid());
        contato.setNome(this.getNome());
        contato.setTelefone(this.getTelefone());

        return contato;

    }
}






