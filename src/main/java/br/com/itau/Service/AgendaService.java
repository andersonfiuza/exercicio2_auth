package br.com.itau.Service;


import br.com.itau.DTO.ContatoEntradaDTO;
import br.com.itau.Model.Contato;
import br.com.itau.Repository.AgendaRepository;
import br.com.itau.Security.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AgendaService {


    @Autowired
    AgendaRepository agendaRepository;

    public Contato criarNovoContato(ContatoEntradaDTO contadoEntradaDTO, Log log) {

        contadoEntradaDTO.setUsuarioid(log.getId());

        Contato contato = contadoEntradaDTO.ConverterParaContato();
        return agendaRepository.save(contato);


    }

    public List<Contato> buscarContato(int usuarioid) {

       return  agendaRepository.findAllByUsuarioid(usuarioid);


    }
}
