package br.com.itau.Controller;


import br.com.itau.DTO.ContatoEntradaDTO;
import br.com.itau.Model.Contato;
import br.com.itau.Security.Log;
import br.com.itau.Service.AgendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class AgendaController {


@Autowired
    AgendaService agendaService;

    @PostMapping
    public Contato incluirContato(@RequestBody @Valid ContatoEntradaDTO contatoEntradaDTO , @AuthenticationPrincipal Log log      ) {


        return  agendaService.criarNovoContato(contatoEntradaDTO,log    );
    }



    @GetMapping(value = "/{usuarioid}")
    public List<Contato> buscarContatoPeloUsuario(@RequestBody @PathVariable(name = "usuarioid") int usuarioid) {


        return  agendaService.buscarContato(usuarioid);
    }


}
