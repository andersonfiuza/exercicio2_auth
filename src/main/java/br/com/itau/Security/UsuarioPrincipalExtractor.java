package br.com.itau.Security;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

import java.util.Map;

public class UsuarioPrincipalExtractor implements PrincipalExtractor {

    @Override
    public Object extractPrincipal(Map<String,Object> map) {

        Log log = new Log();

        log.setId((Integer) map.get("id"));
        log.setName((String) map.get("name"));

        return log;


    }
}
