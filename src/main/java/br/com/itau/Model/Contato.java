package br.com.itau.Model;


import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Contato {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int usuarioid;



    public int getUsuarioid() {
        return usuarioid;
    }

    public void setUsuarioid(int usuarioid) {
        this.usuarioid = usuarioid;
    }


    public String getTelefone() {
        return telefone;
    }

    public Contato() {
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @NotBlank(message = "Favor informar o Nome!")
    private String nome;

    @NotBlank(message = "Favor informar o telefone!")
    private String telefone;


    public Contato(String telefone) {
        this.telefone = telefone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
