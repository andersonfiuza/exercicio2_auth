package br.com.itau.Repository;

import br.com.itau.Model.Contato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgendaRepository  extends CrudRepository<Contato, Integer> {

    List<Contato> findAllByUsuarioid(int usuarioid);
}
